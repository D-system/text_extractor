package main

import (
	"crypto/md5"
	"fmt"
	"io"
	"log"
	"os"
	"regexp"
	"strings"

	"golang.org/x/net/html"
)

var nodeAttrsToTranslate = map[string][]string{
	"img": []string{"alt", "title"},
}

type translation struct {
	key  string
	text string
}

type translations struct {
	translationList []translation
}

func (i18n *translations) addTranslation(key string, text string) {
	tr := translation{key, text}
	i18n.translationList = append(i18n.translationList, tr)
}

func isOnlyWriteChar(str string) bool {
	reg, _ := regexp.Compile("^[ \n\t\r]+$")
	val := reg.MatchString(str)
	return val
}

func contains(a []string, x string) bool {
	for _, n := range a {
		if x == n {
			return true
		}
	}
	return false
}

// For the unique key, it use a part of the string itself and a digest (MD5) of the whole string.
// Only letters and numbers are used. Other characters are replace by "_".
// That nemly string is limited to 20 characters follow by "-" then the digest for the full key.
func generateKey(s string) string {
	digest := md5.Sum([]byte(s))

	str := strings.TrimSpace(s)
	lowered := strings.ToLower(str)

	re := regexp.MustCompile(`[^a-zA-Z0-9]`)
	key := re.ReplaceAllString(lowered, `_`)

	length := 20
	if len(key) < length {
		length = len(key)
	}

	return key[:length] + "-" + fmt.Sprintf("%x", digest)
}

func decorateKey(key string) string {
	return html.UnescapeString(fmt.Sprintf("{{ I18n(`%s`) }}", key))
}

func isTranslated(s string) bool {
	re := regexp.MustCompile(`{{.?I18n.*}}`)
	matched := re.MatchString(s)

	return matched
}

func changeTextTranslation(node *html.Node, i18n *translations) {
	if isTranslated(node.Data) {
		return
	}

	key := generateKey(node.Data)
	i18n.addTranslation(key, node.Data)
	node.Data = decorateKey(key)
}

func changeAttrTranslation(node *html.Node, iAttr int, i18n *translations) {
	if isTranslated(node.Attr[iAttr].Val) {
		return
	}

	key := generateKey(node.Attr[iAttr].Val)
	i18n.addTranslation(key, node.Attr[iAttr].Val)
	node.Attr[iAttr].Val = decorateKey(key)
}

func recursiveParser(node *html.Node, i18n *translations) {
	if node.Type == html.TextNode && !isOnlyWriteChar(node.Data) {
		changeTextTranslation(node, i18n)
	}
	if node.Type == html.ElementNode {
		fieldsToTranslate := nodeAttrsToTranslate[node.Data]
		if fieldsToTranslate != nil {
			for iAttr, a := range node.Attr {
				if contains(fieldsToTranslate, a.Key) {
					changeAttrTranslation(node, iAttr, i18n)
				}
			}
		}
	}
	for c := node.FirstChild; c != nil; c = c.NextSibling {
		recursiveParser(c, i18n)
	}
}

func replaceTextWithI18n(in *strings.Reader, out io.Writer, i18n *translations) {
	nodes, err := html.ParseFragment(in, &html.Node{Type: html.ElementNode})
	if err != nil {
		log.Fatal(err)
	}

	for _, node := range nodes {
		recursiveParser(node, i18n)
		html.Render(out, node)
	}
}

func main() {
	htmlString := `<section class="relative bg-black antialiased text-white overflow-hidden">
	  <div class="dark-overlay"></div>
	  <div class="absolute pin-t pin-l w-full h-full">
	    <img class="background-cover lazyload fade-in faded"
	      src="data:image/jpeg;base64,%2F9j%2F4AAQSkZJRgABAQAASABIAAD%2F2wBDABALDA4MChAODQ4SERATGCgaGBYWGDEjJR0oOjM9PDkzODdASFxOQERXRTc4UG1RV19iZ2hnPk1xeXBkeFxlZ2P%2F2wBDARESEhgVGC8aGi9jQjhCY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2P%2FwAARCAAIABADASIAAhEBAxEB%2F8QAFgABAQEAAAAAAAAAAAAAAAAAAAQG%2F8QAHxAAAQQCAgMAAAAAAAAAAAAAAQIDBBEAEgUhIjGh%2F8QAFAEBAAAAAAAAAAAAAAAAAAAAAf%2FEABkRAAIDAQAAAAAAAAAAAAAAAAABAhEhMf%2FaAAwDAQACEQMRAD8AzTbkNaygRoqQs13sft3lL8Xj4qqdjMihsfJZPoGqB6xjC%2BAo6z%2F%2F2Q%3D%3D"
	      alt="Image of Foc Cover Future Commerce Retail" title="Foc Cover Future Commerce Retail" />
		<p class="text-lg2">In our latest collection, retail and supply chain leaders tell us what the future of retail has in store (and online).</p>
	  </div>
	  <div class="container container-x relative z-20">
	    <div class="flex-column-wrap">
	      <div class="flex-column w-full xl:w-5/6 pt-32 mb-16">
		<p class="text-sm font-bold"><span class="mr-16">Blog posts</span><span class="text-grey">February 28, 2019</span></p>
		<h1>16 Retail &amp; Logistics Experts Weigh In on the Future of Commerce</h1>
		<p class="text-lg">{{ I18n(` + "`in_our_latest_collec-ab91e383be7d7a0211aaf60b54355f85`" + `) }}</p>
	      </div>
	    </div>
	  </div>
	</section>`

	i18n := &translations{}

	htmlIo := strings.NewReader(htmlString)
	replaceTextWithI18n(htmlIo, os.Stdout, i18n)

	fmt.Println("\n\n\nKey/text to translate:")
	fmt.Println(i18n.translationList)
}
